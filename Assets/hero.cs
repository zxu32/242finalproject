﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hero : MonoBehaviour {

    // the initial basic information for a hero
    private int hp = 0;
    private int attack = 0;
    private int defense = 0;
    private int level = 0;
    private int exp = 0;
    private int gold = 0;
    private int keyBlue = 0;
    private int keyYellow = 0;
    private int keyRed = 0;

    // return the hp status
    public int GetHp()
    {
        return hp;
    }
    // return the attack status
    public int GetAttack()
    {
        return attack;
    }
    // return the defense status
    public int GetDefense()
    {
        return defense;
    }
    // return the level status
    public int GetLvl()
    {
        return level;
    }
    // return the exp status
    public int GetExp()
    {
        return exp;
    }
    // return the gold status
    public int GetGold()
    {
        return hp;
    }
    // return the keyBlue status
    public int GetKeyBlue()
    {
        return keyBlue;
    }
    // return the keyYellow status
    public int GetKeyYellow()
    {
        return keyYellow;
    }
    // return the keyRed status
    public int GetKeyRed()
    {
        return keyRed;
    }
    // add gold to hero
    void SetGold(int amount)
    {
        gold = gold + amount;
    }
    // add gold to hero
    void SetExp(int amount)
    {
        exp = exp + amount;
    }
    // enhance attack damage when the hero gets a red gem
    void SetAttack(int amount)
    {
        attack = attack + amount;
    }
    // enhance defense when the hero gets a blue gem
    void SetDefense(int amount)
    {
        defense = defense + amount;
    }
    // change hp, if the hero drinks potion, he gains hp, if he battles a monster he loses hp
    void SetHp(int amount)
    {
        hp = hp + amount;
    }
    // level up
    void SetLevel()
    {
        if(exp >= 100)
        {
            exp = exp - 100;
            level = level + 1;
            attack = attack + 10;
            defense = defense + 10;
        }
    }
    // get a key and identify the type
    void SetKey(string type)
    {
        int comparison = string.Compare(type, "yellow", ignoreCase: true);
        if (comparison == 0)
        {
            keyYellow = keyYellow + 1;
            return;
        }
        comparison = string.Compare(type, "red", ignoreCase: true);
        if (comparison == 0)
        {
            keyRed = keyRed + 1;
            return;
        }
        comparison = string.Compare(type, "blue", ignoreCase: true);
        if (comparison == 0)
        {
            keyBlue = keyBlue + 1;
            return;
        }
    }


    // Use this for initialization
    void Start () {
        hp = 1000;
        attack = 10;
        defense = 10;
        level = 1;
        exp = 0;
        gold = 0;
        keyBlue = 0;
        keyYellow = 0;
        keyRed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
